import { Component } from '@angular/core';

@Component({
  selector: 'app-not-found',
  template: `<h1>Not found!</h1>`,
  styles: [`
    h1 {
      color: white;
      font-size: 80px;
      font-style: italic;
      padding: 80px;
      text-align: center;
    }
  `]
})
export class NotFoundComponent {}
