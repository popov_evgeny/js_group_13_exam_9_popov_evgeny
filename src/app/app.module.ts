import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { FormComponent } from './form/form.component';
import { ToolbarComponent } from './ui/toolbar/toolbar.component';
import { FooterComponent } from './ui/footer/footer.component';
import { NotFoundComponent } from './not-found.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CocktailService } from './shared/cocktail.service';
import { ModalComponent } from './ui/modal/modal.component';
import { ValidatorImageUrlDirective } from './validator-image-url.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FormComponent,
    ToolbarComponent,
    FooterComponent,
    NotFoundComponent,
    ModalComponent,
    ValidatorImageUrlDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [CocktailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
