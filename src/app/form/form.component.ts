import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { CocktailModel } from '../shared/cocktail.model';
import { CocktailService } from '../shared/cocktail.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { imageUrlValidator } from '../validator-image-url.directive';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit, OnDestroy {
  reactiveForm!: FormGroup;
  addLoadingSubscription!: Subscription;
  loading!: boolean;
  isAddedIng = true;

  constructor(
    private cocktailService: CocktailService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.reactiveForm = new FormGroup({
      name: new FormControl('', Validators.required),
      imgUrl: new FormControl('', [Validators.required, imageUrlValidator]),
      cocktailType: new FormControl('', Validators.required),
      description: new FormControl(''),
      instruction: new FormControl('',Validators.required),
      ingredients: new FormArray([])});
    this.addLoadingSubscription = this.cocktailService.addCocktailLoading.subscribe((addLoading: boolean) => {
      this.loading = addLoading;
    });
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.reactiveForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  fieldIngredientsError(fieldName: string, index: number, errorType: string) {
    const ingredients =<FormArray>this.reactiveForm.get('ingredients');
    const field = ingredients.controls[index].get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }


  addIng() {
    this.isAddedIng = false;
    const ingredients = <FormArray>this.reactiveForm.get('ingredients');
    const ing = new FormGroup({
      ingName: new FormControl('', Validators.required),
      amount: new FormControl(null, Validators.required),
      countIng: new FormControl('', Validators.required),
    });
    ingredients.push(ing);
  }

  getIngArray() {
    return (<FormArray> this.reactiveForm.get('ingredients')).controls
  }

  onSubmit() {
    const id = Math.random().toString();
    const cocktail = new CocktailModel(
      id, this.reactiveForm.value.name,
      this.reactiveForm.value.imgUrl,
      this.reactiveForm.value.cocktailType,
      this.reactiveForm.value.description,
      this.reactiveForm.value.ingredients,
      this.reactiveForm.value.instruction,);
    this.cocktailService.addCocktail(cocktail).subscribe(() => { void this.router.navigate(['/']) });
  }

  onRemoveIng(index: number) {
    const formArray = <FormArray>this.reactiveForm.get('ingredients');
    formArray.removeAt(index);
  }

  ngOnDestroy() {
    this.addLoadingSubscription.unsubscribe();
  }
}
