export class CocktailModel {
  constructor(
    public id: string,
    public name: string,
    public imageUrl: string,
    public cocktailType: string,
    public description: string,
    public ingredients: Ingredients [],
    public instruction: string
  ) {}
}

export class Ingredients {
  constructor(
    public ingName: string,
    public countIng: string,
    public amount: number
  ) {}
}
