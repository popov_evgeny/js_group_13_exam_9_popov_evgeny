import { Injectable } from '@angular/core';
import { CocktailModel } from './cocktail.model';
import { Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map, tap } from 'rxjs/operators';

@Injectable()

export class CocktailService {
  private cocktailsArray: CocktailModel[] | null = null;
  changeArrayCocktails = new Subject<CocktailModel[]>();
  fetchingArrayCocktailsLoading = new Subject<boolean>();
  addCocktailLoading = new Subject<boolean>();

  constructor(
    private http: HttpClient
  ) {}


  fetchCocktails() {
    this.fetchingArrayCocktailsLoading.next(true);
    this.http.get<{ [id: string]: CocktailModel }>(`https://project-f65ad-default-rtdb.firebaseio.com/cocktails.json`).pipe(map(result => {
      if (!result) {
        return [];
      }
      return Object.keys(result).map(id => {
        const cocktail = result[id];
        return new CocktailModel(id, cocktail.name, cocktail.imageUrl, cocktail.cocktailType, cocktail.description, cocktail.ingredients, cocktail.instruction);
      })
    })).subscribe(cocktail => {
      this.cocktailsArray = cocktail;
      this.changeArrayCocktails.next(this.cocktailsArray.slice());
      this.fetchingArrayCocktailsLoading.next(false);
    }, () => {
      this.fetchingArrayCocktailsLoading.next(false);
    });
  }


  addCocktail(cocktail: CocktailModel) {
    this.addCocktailLoading.next(true);
    const body = {
      name: cocktail.name,
      imageUrl: cocktail.imageUrl,
      cocktailType: cocktail.cocktailType,
      description: cocktail.description,
      ingredients: cocktail.ingredients,
      instruction: cocktail.instruction,
    };
    return this.http.post('https://project-f65ad-default-rtdb.firebaseio.com/cocktails.json', body).pipe(tap(() => {
      this.addCocktailLoading.next(false);
    }, () => {
      this.addCocktailLoading.next(false);
    }));
  }
}
