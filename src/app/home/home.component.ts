import { Component, OnDestroy, OnInit } from '@angular/core';
import { CocktailModel } from '../shared/cocktail.model';
import { CocktailService } from '../shared/cocktail.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  cocktailsArray!: CocktailModel[];
  cocktail: CocktailModel | null = null;
  loadingSubscription!: Subscription;
  fetchCocktailsSubscription!: Subscription;
  loading!: boolean;
  isOpenModal = false;

  constructor( private cocktailService: CocktailService ) { }

  ngOnInit(): void {
    this.fetchCocktailsSubscription = this.cocktailService.changeArrayCocktails.subscribe((cocktailsArray: CocktailModel[]) => {
        this.cocktailsArray = cocktailsArray;
    });
    this.loadingSubscription = this.cocktailService.fetchingArrayCocktailsLoading.subscribe((loading: boolean) => {
      this.loading = loading;
    });
    this.cocktailService.fetchCocktails();
  }

  openModal(id: string) {
    this.isOpenModal = true;
    this.cocktailsArray.forEach((cocktail:CocktailModel) => {
      if (cocktail.id === id){
        this.cocktail = cocktail;
      }
    })
  }

  isClose() {
    this.isOpenModal = false;
  }

  ngOnDestroy() {
    this.fetchCocktailsSubscription.unsubscribe();
    this.loadingSubscription.unsubscribe();
  }
}
