import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
import { Directive } from '@angular/core';

export const imageUrlValidator = (): ValidatorFn => {
  return (control: AbstractControl): ValidationErrors | null => {
    const imageUrl = /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&/=]*)/.test(control.value);
    if (imageUrl) {
      return null;
    }
    return {imgUrl: true};
  }
}

@Directive({
  selector: '[appImageUrlValidator]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidatorImageUrlDirective,
    multi: true
  }]
})

export class ValidatorImageUrlDirective implements Validator{
  validate(control: AbstractControl): ValidationErrors | null {
    return imageUrlValidator()(control);
  }
}
